const http = require('http')
const express = require('express')
const path = require('path')
// const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const cors = require('cors')

const debug = require('debug')('payconiq-rest-mvp:rest-server')

export class RESTServer {
  constructor (environment) {
    this.createExpressApp()

    this.setupLogging(environment)

    this.setupAPIEndpoints()

    this.setupErrorHandlingEndpoints()
  }

  listen (port) {
    this.server.listen(port)
    this.server.on('error', onError)
    this.server.on('listening', onListening)

    function onError (error) {
      if (error.syscall !== 'listen') {
        throw error
      }

      const bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port

      // handle specific listen errors with friendly messages
      switch (error.code) {
        case 'EACCES':
          console.error(bind + ' requires elevated privileges')
          process.exit(1)
        case 'EADDRINUSE':
          console.error(bind + ' is already in use')
          process.exit(1)
        default:
          throw error
      }
    }

    const server = this.server

    function onListening () {
      const addr = server.address()
      const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port
      console.log('\nListening on ' + bind)
    }
  }

  createExpressApp () {
    this.app = express()

    this.app.use(bodyParser.json())
    this.app.use(bodyParser.urlencoded({
      extended: false
    }))

    this.app.use(cookieParser())

    this.server = http.createServer(this.app)
  }

  setupLogging (environment) {
    // express
    this.app.use(logger('dev'))
    if (environment === 'local') {
      // business code
      process.env.DEBUG = 'payconiq-rest-mvp:*'
      require('debug').enable(process.env.DEBUG)

      // cors enabled in non-prod
      this.app.use(cors())
    } else {
      this.app.use(express.static(path.join(__dirname, 'public', 'dist')))
      // this.app.use(favicon(path.join(__dirname, 'public', 'dist', 'favicon.ico')))
    }
  }

  setupAPIEndpoints () {
    const restAPI = require('./api')
    this.app.use('/api', restAPI)
  }

  setupErrorHandlingEndpoints () {
    // catch 404 and forward to error handler
    this.app.use(function (req, res, next) {
      const err = new Error('Not Found')
      err.status = 404
      next(err)
    })

    // default error handler
    this.app.use(function (err, req, res, next) {
      debug(`Error catched! ${err}`)

      const error = {
        status: err.status || 500,
        message: err.message
      }

      res.status(error.status).send(error)
    })
  }
}
