const should = require('should')

export function test (request, server, done) {
  describe('PUT - update stock', () => {
    it('should update lastUpdate field', (done) => {
      const newDate = Date.parse('January, 27, 1981')

      request(server)
        .put('/api/stocks/1')
        .send({
          lastUpdate: newDate
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) {
            done(err)
          }

          res.body.lastUpdate.should.eql(newDate)

          done()
        })
    })

    it('should fail to update non-existing stock', (done) => {
      const newDate = Date.parse('January, 27, 1981')

      request(server)
        .put('/api/stocks/999')
        .send({
          lastUpdate: newDate
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(500)
        .end((err, res) => {
          if (err) {
            done(err)
          }

          should.exist(res.body.status)
          res.body.message.should.eql('Stock with id 999 not found')

          done()
        })
    })
  })
}
