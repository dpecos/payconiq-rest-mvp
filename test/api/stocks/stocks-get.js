const should = require('should')

export function test (request, server, done) {
  describe('GET - retrieve all', () => {
    it('should return all available stocks', () => {
      request(server)
        .get('/api/stocks')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) {
            done(err)
          }

          res.body.length.should.eql(3)

          done()
        })
    })
  })

  describe('GET - retrieve single stock', () => {
    it('should find an existing stock', (done) => {
      request(server)
        .get('/api/stocks/1')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          should.not.exist(err)

          res.body.name.should.eql('AAPL')

          done()
        })
    })

    it('should return an error trying to retrieve unexisting stock', (done) => {
      request(server)
        .get('/api/stocks/999')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(500)
        .end((err, res) => {
          if (err) {
            done(err)
          }

          should.exist(res.body.status)
          res.body.message.should.eql('Stock with id 999 not found')

          done()
        })
    })
  })
}
