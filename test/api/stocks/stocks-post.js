const should = require('should')

export function test (request, server, done) {
  describe('POST - create stock', () => {
    it('should create a new stock', (done) => {
      const newStock = {
        id: 101,
        name: 'NEW',
        currentPrice: 999.0,
        lastUpdate: Date.now()
      }

      request(server)
        .post('/api/stocks')
        .send(newStock)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) {
            done(err)
          }

          res.body.should.deepEqual(newStock)

          done()
        })
    })

    it('should fail to create an stock matching an already existing id', (done) => {
      const newStock = {
        id: 1,
        name: 'NEW',
        currentPrice: 999.0,
        lastUpdate: Date.now()
      }

      request(server)
        .post('/api/stocks')
        .send(newStock)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(500)
        .end((err, res) => {
          if (err) {
            done(err)
          }

          should.exist(res.body.status)
          res.body.message.should.eql('An stock with id 1 already exists')

          done()
        })
    })

    it('should fail to create an incomplete stock', (done) => {
      const newStock = {
        id: 102,
        name: '',
        lastUpdate: Date.now()
      }

      request(server)
        .post('/api/stocks')
        .send(newStock)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(500)
        .end((err, res) => {
          if (err) {
            done(err)
          }

          should.exist(res.body.status)
          res.body.message.should.match(/doesn't match Swagger contract/)

          done()
        })
    })
  })
}
