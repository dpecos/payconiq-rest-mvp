import * as getTests from './stocks/stocks-get'
import * as postTests from './stocks/stocks-post'
import * as putTests from './stocks/stocks-put'
import { RESTServer } from '../../rest-server'

const request = require('supertest')

const restServer = new RESTServer(process.env.NODE_ENV)

describe('REST API', () => {
  describe('general', () => {
    it('should return 404 for unknown URLs', (done) => {
      request(restServer.server)
        .get('/error-404')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(404)
        .end(done)
    })
  })

  describe('stocks', (done) => {
    getTests.test(request, restServer.server, done)
    postTests.test(request, restServer.server, done)
    putTests.test(request, restServer.server, done)
  })
})
