import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getTableHeaders() {
    return element.all(by.css('.ui-datatable th span')).map(el => el.getText());
  }
}
