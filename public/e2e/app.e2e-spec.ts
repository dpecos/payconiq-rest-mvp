import { AppPage } from './app.po';

describe('payconiq-mvp App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should have a table with 4 headers', () => {
    page.navigateTo();
    const headers = page.getTableHeaders();
    expect(headers).toContain('ID');
    expect(headers).toContain('Name');
    expect(headers).toContain('Current Price');
    expect(headers).toContain('Last Update');
  });
});
