import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Stock } from '../models/stock.model';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class StocksService {

    constructor(private http: Http) { }

    async getStocks(): Promise<Stock[]> {

        const response = await new Promise<Stock[]>(resolve => {
            this.http.get(`${environment.apiUrl}/stocks`).subscribe(
                res => { resolve(res.json().map(s => { s.lastUpdate = new Date(s.lastUpdate); return s; })); },
                err => { resolve(err); }
            );
        });

        return response;
    }

    updateStock(stock: Stock): Promise<Response> {
        const data = {
            lastUpdate: stock.lastUpdate.getTime()
        };
        return this.http.put(`${environment.apiUrl}/stocks/${stock.id}`, data).toPromise();
    }

    createStock(stock: Stock): Promise<Response> {
        const newStock = this.checkStockTypes(stock);
        return this.http.post(`${environment.apiUrl}/stocks`, newStock).toPromise();
    }

    private checkStockTypes(stock) {
        const newStock = {
            id: parseInt(stock.id, 10),
            name: stock.name,
            currentPrice: parseInt(stock.currentPrice, 10),
            lastUpdate: stock.lastUpdate != null ? stock.lastUpdate.getTime() : null
        };
        return newStock;
    }
}
