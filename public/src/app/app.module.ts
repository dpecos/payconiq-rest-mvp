import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { DataTableModule, CalendarModule, ButtonModule, InputTextModule, DialogModule, GrowlModule } from 'primeng/primeng';

import { AppComponent } from './components/app.component';
import { StocksService } from './services/stocks.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    DataTableModule,
    CalendarModule,
    ButtonModule,
    InputTextModule,
    DialogModule,
    GrowlModule
  ],
  providers: [StocksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
