import { Component, OnInit } from '@angular/core';
import {Message} from 'primeng/primeng';

import { Stock } from '../models/stock.model';
import { StocksService } from '../services/stocks.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  stocks: Stock[];

  stockFormVisible = false;
  newStock: Stock = <Stock>{};

  msgs: Message[] = [];

  constructor(private stockService: StocksService) { }

  ngOnInit() {
    this.loadStocks();
  }

  async loadStocks() {
    this.stocks = await this.stockService.getStocks();
  }

  async updateStock(stock: Stock) {
    await this.stockService.updateStock(stock);
  }

  showStockForm() {
    this.newStock = <Stock>{};
    this.stockFormVisible = true;
  }

  async addNewStock() {
    try {
      await this.stockService.createStock(this.newStock);
      this.stockFormVisible = false;
      this.msgs = [{ severity: 'success', summary: 'Success', detail: 'Stock created!' }];
      this.loadStocks();
    } catch (err) {
      console.log(err);
      this.msgs = [{ severity: 'error', summary: err.statusText, detail: JSON.parse(err._body).message }];
    }
  }
}
