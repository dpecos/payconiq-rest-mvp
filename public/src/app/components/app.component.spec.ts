import { TestBed, async } from '@angular/core/testing';

import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { DataTableModule, CalendarModule, ButtonModule, InputTextModule, DialogModule, GrowlModule } from 'primeng/primeng';

import { AppComponent } from './app.component';
import { StocksService } from '../services/stocks.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpModule,
        FormsModule,
        DataTableModule,
        CalendarModule,
        ButtonModule,
        InputTextModule,
        DialogModule,
        GrowlModule
      ],
      providers: [StocksService],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
