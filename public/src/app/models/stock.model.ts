export interface Stock {
    id: number;
    name: string;
    currentPrice: number;
    lastUpdate: Date;
}
