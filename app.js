import { RESTServer } from './rest-server'

const restServer = new RESTServer(process.env.NODE_ENV)
restServer.listen(3000)
