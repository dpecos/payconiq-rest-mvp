const express = require('express')
const router = express.Router()

router.use('/stocks', require('./controllers/stocks'))
router.use('/docs', require('./controllers/swagger').router)

module.exports = router
