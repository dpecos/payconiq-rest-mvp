const express = require('express')
const router = express.Router()

const debug = require('debug')('payconiq-rest-mvp:swagger')

const swaggerJSDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')

const options = {
  swaggerDefinition: {
    info: {
      title: 'Payconiq MVP',
      version: '1.0.0',
      description: 'REST API for the Payconiq Stocks MVP',
      contact: {
        email: 'dpecos@gmail.com'
      }
    },
    tags: [
      {
        name: 'stocks',
        description: 'Stocks API'
      }
    ],
    schemes: [ 'http' ],
    host: 'localhost:3000',
    basePath: '/api'
  },
  apis: ['./api/controllers/stocks.js', './api/models/stock-model.js']
}

const swaggerSpec = swaggerJSDoc(options)
require('swagger-model-validator')(swaggerSpec)

router.get('/json', function (req, res) {
  res.setHeader('Content-Type', 'application/json')
  res.send(swaggerSpec)
})

router.use('/', swaggerUi.serve, swaggerUi.setup(swaggerSpec))

function validateModel (name, model) {
  const responseValidation = swaggerSpec.validateModel(name, model, false, true)
  if (!responseValidation.valid) {
    debug(responseValidation.errors)
    throw new Error(`Model doesn't match Swagger contract`)
  }
}

module.exports = {
  router,
  validateModel
}
